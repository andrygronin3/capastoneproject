from sqlalchemy.orm import sessionmaker, joinedload

from parser_orchestrator.models.InterfaceForWorkWithData import IRegistryDataRepository
from parser_orchestrator.models.base import db_connect
from parser_orchestrator.parsers.quotes_use_as_reference.models import Quote


class QuotesRepository(IRegistryDataRepository):
    def get_all_entries(self):
        engine = db_connect()
        session = sessionmaker(bind=engine)()
        result = session.query(Quote).options(joinedload('*')).all()
        session.close()
        return result;