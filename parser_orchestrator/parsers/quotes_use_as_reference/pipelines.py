from sqlalchemy.orm import sessionmaker
from scrapy.exceptions import DropItem


import logging

from parser_orchestrator.models.PipelineBase import BasePipeline
from parser_orchestrator.models.base import db_connect, create_table
from parser_orchestrator.parsers.quotes_use_as_reference.models import Quote, Author, Tag


class DuplicatesPipeline(BasePipeline):

    def __init__(self):
        super().__init__()

    def process_item(self, item, spider):
        return self.process_item_base(item, spider, Quote, Quote.quote_content == item["quote_content"])


class SavePipeline(object):
    def __init__(self):
        engine = db_connect()
        create_table(engine)
        self.Session = sessionmaker(bind=engine)
        logging.info("****SaveQuotePipeline: database connected****")


    def process_item(self, item, spider):
        session = self.Session()
        quote = Quote()
        author = Author()
        author.name = item["author_name"]
        author.birthday = item["author_birthday"]
        author.bornlocation = item["author_bornlocation"]
        author.bio = item["author_bio"]
        quote.quote_content = item["quote_content"]

        exist_author = session.query(Author).filter_by(name = author.name).first()
        if exist_author is not None:
            quote.author = exist_author
        else:
            quote.author = author

        if "tags" in item:
            for tag_name in item["tags"]:
                tag = Tag(name=tag_name)
                # check whether the current tag already exists in the database
                exist_tag = session.query(Tag).filter_by(name = tag.name).first()
                if exist_tag is not None:  # the current tag exists
                    tag = exist_tag
                quote.tags.append(tag)

        try:
            session.add(quote)
            session.commit()

        except:
            session.rollback()
            raise

        finally:
            session.close()

