from sqlalchemy.orm import relationship

from parser_orchestrator.models.base import Base
from sqlalchemy import Column, DateTime, NVARCHAR, ForeignKey, Table
from sqlalchemy import (
    Integer)

quote_tag = Table('eac_registry_entry_to_class', Base.metadata,
                  Column('entry_id', Integer, ForeignKey('eac_registry_eac_registry_entry.id')),
                  Column('class_id', Integer, ForeignKey('eac_registry_software_class.id'))
                  )


class EacRegistryEntry(Base):
    __tablename__ = "eac_registry_eac_registry_entry"

    id = Column(Integer, primary_key=True)
    registration_number = Column(NVARCHAR(1000))
    software_name = Column(NVARCHAR(1000))
    registration_date = Column(DateTime)
    classes = relationship('SoftwareClass', secondary='eac_registry_entry_to_class',
                           backref="eac_registry_eac_registry_entry")


class SoftwareClass(Base):
    __tablename__ = "eac_registry_software_class"
    id = Column(Integer, primary_key=True)
    code = Column(NVARCHAR(1000), unique=True)
    description = Column(NVARCHAR(1000))
