from sqlalchemy.orm import sessionmaker, joinedload

from parser_orchestrator.models.InterfaceForWorkWithData import IRegistryDataRepository
from parser_orchestrator.models.base import db_connect
from parser_orchestrator.parsers.eac_registry.models import EacRegistryEntry


class EacRegistryRepository(IRegistryDataRepository):
    def get_all_entries(self):
        engine = db_connect()
        session = sessionmaker(bind=engine)()
        result = session.query(EacRegistryEntry).options(joinedload('*')).all()
        return result;