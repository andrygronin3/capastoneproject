import logging
from datetime import datetime

import scrapy
from sqlalchemy.orm import sessionmaker

from parser_orchestrator.long_running_task.SqlLogHandler import SQLiteHandler
from parser_orchestrator.models.base import db_connect
from parser_orchestrator.parsers.eac_registry.items import EacRegistryItem, SoftwareClassDto


def convert_date(text):
    # convert string 19.09.2019 to Python date
    return datetime.strptime(text, '%d.%m.%Y')


class EacRegistrySpider(scrapy.Spider):
    name = "EacRegistry"
    id_field_name = "registration_number"
    start_urls = [
        'https://eac-reestr.digital.gov.ru/reestr/?PAGE_N=1&PAGE_S=100',
    ]
    custom_settings = {
        "ITEM_PIPELINES": {
        "parser_orchestrator.parsers.eac_registry.pipelines.DuplicatesPipeline": 100,
        "parser_orchestrator.parsers.eac_registry.pipelines.SavePipeline": 200,
        }
    }
    def __init__(self):
        super(EacRegistrySpider, self).__init__()
        engine = db_connect()
        self.Session = sessionmaker(bind=engine)
        #logger = logging.getLogger()
        #handler = SQLiteHandler(self.Session(), self.name)
        #handler.setFormatter(logging.Formatter("%(asctime)s"))
        #logger.addHandler(handler)

    def parse(self, response):
        for entry in response.css("tr.a-link"):
            reg_number = entry.css("td:nth-child(1)::text").get()
            name = entry.css("td:nth-child(2)::text").get()
            class_code_and_description = entry.css("td td::text").getall()
            reg_date = entry.css("td:nth_child(4)::text").get()

            class_code_and_desc_paired = []
            for i in range(0, len(class_code_and_description) - 1, 2):
                class_code_and_desc_paired.append(
                    SoftwareClassDto(class_code_and_description[i], class_code_and_description[i + 1]))

            yield EacRegistryItem(
                registration_number=reg_number,
                software_name=name,
                registration_date=convert_date(reg_date),
                classes=class_code_and_desc_paired)

        next_page = response.css('a[title="Вперед"]::attr(href)')
        if len(next_page) != 0:
            next_page_url = response.urljoin(next_page.extract()[0])
            yield scrapy.Request(next_page_url, callback=self.parse)
