# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import json

from sqlalchemy.orm import sessionmaker
from scrapy.exceptions import DropItem


import logging

from parser_orchestrator.long_running_task.SqlLogHandler import SQLiteHandler
from parser_orchestrator.models.PipelineBase import BasePipeline
from parser_orchestrator.models.base import db_connect, create_table
from parser_orchestrator.parsers.eac_registry.models import EacRegistryEntry, SoftwareClass


class DuplicatesPipeline(BasePipeline):

    def __init__(self):
        super().__init__()

    def process_item(self, item, spider):
        return self.process_item_base(item, spider, EacRegistryEntry, EacRegistryEntry.registration_number == item["registration_number"])



class SavePipeline(object):
    def __init__(self):
        engine = db_connect()
        create_table(engine)
        self.Session = sessionmaker(bind=engine)
        #logger = logging.getLogger()
        #handler = SQLiteHandler(self.Session())
        #handler.setFormatter(logging.Formatter("%(asctime)s"))
        #logger.addHandler(handler)
        #logger.warning("This is a warning")

    def process_item(self, item, spider):
        session = self.Session()
        eac_registry_entry = EacRegistryEntry()

        eac_registry_entry.registration_number = item["registration_number"]
        eac_registry_entry.software_name = item["software_name"]
        eac_registry_entry.registration_date = item["registration_date"]

        for _class in item["classes"]:
            exist_class = session.query(SoftwareClass).filter_by(code=_class.code).first()
            if exist_class is not None:
                eac_registry_entry.classes.append(exist_class)
            else:
                eac_registry_entry.classes.append(SoftwareClass(code=_class.code, description=_class.description))

        try:
            session.add(eac_registry_entry)
            session.commit()
        except:
            session.rollback()
            raise

        finally:
            session.close()

