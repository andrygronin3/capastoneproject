from dataclasses import dataclass

from scrapy.item import Item, Field
from scrapy.loader.processors import MapCompose
from datetime import datetime

def convert_date(text):
    # convert string 19.09.2019 to Python date
    return datetime.strptime(text, '%d.%B.%Y')

class NostroyRegistryItem(Item):
    id = Field()
    full_name = Field()
    registration_date = Field(input_processor=MapCompose(convert_date))
    job_type = Field(input_processor=MapCompose(convert_date))
    status = Field()
