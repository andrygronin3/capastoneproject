from datetime import datetime
import pytesseract
import requests

import scrapy

from PIL import Image
from io import BytesIO

import os


from parser_orchestrator.parsers.nostroy_registry.items import NostroyRegistryItem


def convert_date(text):
    # convert string 19.09.2019 to Python date
    return datetime.strptime(text, '%d.%m.%Y')


class NostroyRegistrySpider(scrapy.Spider):
    name = "NostroyRegistry"
    start_urls = [
        'http://nrs.nostroy.ru',
    ]
    '''id_field_name = "registration_number"
    start_urls = [
        'https://eac-reestr.digital.gov.ru/reestr/?PAGE_N=1&PAGE_S=100',
    ]
    custom_settings = {
        "ITEM_PIPELINES": {
        "parser_orchestrator.parsers.eac_registry.pipelines.DuplicatesPipeline": 100,
        "parser_orchestrator.parsers.eac_registry.pipelines.SavePipeline": 200,
        }
    }'''

    def img_to_string(self, img_url:str, one_line: bool):
        response = requests.get(img_url)
        png = Image.open(BytesIO(response.content)).convert('RGBA')
        background = Image.new('RGBA', png.size, (255, 255, 255))
        alpha_composite = Image.alpha_composite(background, png)
        current_path = os.path.dirname(os.path.abspath(__file__))
        pytesseract.pytesseract.tesseract_cmd = current_path + r'\teseract\tesseract'
        if one_line:
            return pytesseract.image_to_string(alpha_composite, config='--psm 7')
        else:
            return pytesseract.image_to_string(alpha_composite, lang="rus" )
        

    def parse(self, response):
        for line in response.css("tbody>tr")[1:]:

            reg_number_url = line.css("td:nth-child(1)>img::attr(src)").get()
            id = self.img_to_string(self.start_urls[0] + reg_number_url[2:], one_line=True)

            full_name_url = line.css("td:nth-child(2)>img::attr(src)").get()
            full_name = self.img_to_string(self.start_urls[0] + full_name_url[2:], one_line=False)

            registration_date_url = line.css("td:nth-child(3)>img::attr(src)").get()
            registration_date = self.img_to_string(self.start_urls[0] + registration_date_url[2:], one_line=True)

            job_type = line.css("td:nth-child(5)::text").get()

            status = line.css("td:nth-child(6)::text").get()

            item = NostroyRegistryItem(
                id=id,
                full_name=full_name,
                registration_date=registration_date,
                job_type=job_type,
                status=status
            )
            '''reg_number = entry.css("td:nth-child(1)::text").get()
            name = entry.css("td:nth-child(2)::text").get()
            class_code_and_description = entry.css("td td::text").getall()
            reg_date = entry.css("td:nth_child(4)::text").get()

            class_code_and_desc_paired = []
            for i in range(0, len(class_code_and_description) - 1, 2):
                class_code_and_desc_paired.append(
                    SoftwareClassDto(class_code_and_description[i], class_code_and_description[i + 1]))'''

            yield item;

        next_page = response.css('a[title="Вперед"]::attr(href)')
        if len(next_page) != 0:
            next_page_url = response.urljoin(next_page.extract()[0])
            yield scrapy.Request(next_page_url, callback=self.parse)
