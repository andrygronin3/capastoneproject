BOT_NAME = 'parser_orchestrator'

SPIDER_MODULES = [
#'parser_orchestrator.parsers.eac_registry.spider',
#                  'parser_orchestrator.parsers.quotes_use_as_reference.spider',
#                  'parser_orchestrator.parsers.nostroy_registry.spider'
                  ]
NEWSPIDER_MODULE = 'parser_orchestrator.spiders'


# SQLite
#CONNECTION_STRING = 'sqlite:///scrapy_quotes.db'
# MySQL
CONNECTION_STRING = "{drivername}://{user}:{passwd}@{host}:{port}/{db_name}?charset=utf8".format(
     drivername="mysql",
     user="root",
     passwd="andrey22",
     host="localhost",
     port="3306",
     db_name="parsingData",
 )


FEED_EXPORT_ENCODING = 'utf-8'

ROBOTSTXT_OBEY = True

# Configure item pipelines
# See https://doc.scrapy.org/en/latest/topics/item-pipeline.html


