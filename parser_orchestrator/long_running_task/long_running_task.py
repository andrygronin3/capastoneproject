import datetime
import logging
import os

from scrapy.crawler import CrawlerRunner
from sqlalchemy.orm import sessionmaker
from twisted.internet import reactor
from twisted.internet.defer import Deferred
from twisted.internet.task import deferLater

from parser_orchestrator.long_running_task.SqlLogHandler import SQLiteHandler
from parser_orchestrator.models.WorkItem import WorkItem
from parser_orchestrator.models.base import db_connect, create_table
from parser_orchestrator.scripts_for_api.get_project_settings_modified import get_project_settings_modified

os.environ.setdefault('SCRAPY_SETTINGS_MODULE', "parser_orchestrator.settings")

def sleep(self, *args, seconds):
    """Non blocking sleep callback"""
    return deferLater(reactor, seconds, lambda: None)


engine = db_connect()
create_table(engine)
sessionmaker = sessionmaker(bind=engine)

process = CrawlerRunner(get_project_settings_modified())


def endlessCrawl(result):
    session = sessionmaker()

    workitems = session.query(WorkItem)\
        .filter_by(completed=False)\
        .filter(WorkItem.next_run <= datetime.datetime.now())
    deferred = Deferred()
    deferred.callback(0)
    for workitem in workitems:

        logger = logging.getLogger()
        handler = SQLiteHandler(session, workitem.spiderName, workitem.id)
        handler.setFormatter(logging.Formatter("%(asctime)s"))
        logger.addHandler(handler)

        deferred = process.crawl(workitem.spiderName)
        if workitem.period is not None:
            workitem.next_run =datetime.datetime.now() + workitem.period
        else:
            workitem.completed = True

        deferred.addCallback(lambda results: logger.removeHandler(handler))

    if len(deferred.callbacks) == 0:
        deferred.addCallback(lambda results: print('no workItems, waiting'))
    else:
        deferred.addCallback(lambda results: print('crawled, waiting'))
    deferred.addCallback(sleep, seconds=10)
    deferred.addCallback(endlessCrawl)
    session.commit()
    session.close()
    return deferred


endlessCrawl(None)
reactor.run()


