import logging
import datetime

from sqlalchemy.orm import Session

from parser_orchestrator.models.LogEntry import LogEntry

DEFAULT_SEPARATOR = '|'
DEFAULT_DATA_TYPE = 'TEXT'


#WARNING: attributes must be choosen from https://docs.python.org/3/library/logging.html#formatter-objects
DEFAULT_ATTRIBUTES_LIST = ['asctime', 'levelname', 'name', 'message']


class SQLiteHandler(logging.Handler):

    def __init__(self, session: Session, spider_name: str, work_item_id: int):
        super().__init__()

        self.session = session

        self.spider_name = spider_name

        self.work_item_id = work_item_id

        # Create table if needed
        #create_table_sql = 'CREATE TABLE IF NOT EXISTS ' + self.table + ' (' + ((' ' + DEFAULT_DATA_TYPE + ', ').join(self.attributes)) + ' ' + DEFAULT_DATA_TYPE + ');'
        #print(create_table_sql)



    def emit(self, record):
        '''
        Save the log record
        Parameters:
            self: instance of the class
            record: log record to be saved
        Returns:
            None
        '''
        # Use default formatting if no formatter is set
        self.format(record)

        #print(record.__dict__)
        #record_values = [record.__dict__[k] for k in self.attributes]
        #str_record_values = ', '.join("'{0}'".format(v.replace("'", '').replace('"', '').replace('\n', ' ')) for v in record_values)
        log_entry = LogEntry()
        log_entry.asctime = datetime.datetime.strptime(record.asctime[:-4:], '%Y-%m-%d %H:%M:%S')
        log_entry.level = record.levelname
        log_entry.message = record.message
        log_entry.spiderName = self.spider_name
        log_entry.workItemId = self.work_item_id

        self.session.add(log_entry)
        self.session.commit()
        self.session.close()


        #insert_sql = 'INSERT INTO ' + self.table + ' (' + (', '.join(self.attributes)) + ') VALUES (' + str_record_values + ');'
        #print(insert_sql)
        #conn = sqlite3.connect(self.database)
        #conn.execute(insert_sql)
        #conn.commit()
        #conn.close()