import datetime
import os
from itertools import groupby
from operator import attrgetter

import uvicorn as uvicorn
from fastapi import FastAPI, HTTPException
from scrapy import spiderloader

from parser_orchestrator.api.DTOs import ScheduleInput, ScheduleOutput, ScheduledRun
from parser_orchestrator.models.RepositoryFactory import RepositoryFactory
from parser_orchestrator.models.WorkItem import WorkItem
from parser_orchestrator.models.base import db_connect, create_table
from sqlalchemy.orm import sessionmaker

from parser_orchestrator.scripts_for_api.get_project_settings_modified import get_project_settings_modified

app = FastAPI()


@app.get("/parsers")
def get_parsers():
    settings = get_project_settings_modified()
    spider_loader = spiderloader.SpiderLoader.from_settings(settings)
    spider_names = spider_loader.list()

    result = []
    for name in spider_names:
        spider = spider_loader.load(name)
        result.append(
            {
                "name": name,
                "start_urls": spider.start_urls
             })
    return result


@app.post("/schedule_parser")
def schedule_parser(parser_name: str, schedule_input: ScheduleInput):
    settings = get_project_settings_modified()
    spider_loader = spiderloader.SpiderLoader.from_settings(settings)
    spider_names = spider_loader.list()

    if parser_name not in spider_names:
        raise HTTPException(status_code=404, detail="Parser with name '" + parser_name + "' doesn't exist")

    engine = db_connect()
    create_table(engine)
    session = sessionmaker(bind=engine)()
    new_work_item = WorkItem(
            spiderName=parser_name,
            next_run=schedule_input.run_date,
            period=schedule_input.run_period)
    session.add(new_work_item)
    session.commit()

    result = {
        "WorkItemId": new_work_item.id
    }

    session.close()
    return result


@app.post("/execute_parser")
def execute_parser(parser_name: str):
    return schedule_parser(parser_name, ScheduleInput(run_date=datetime.datetime.now()))


@app.post("/execute_parser_sync")
def execute_parser(parser_name: str):
    raise NotImplementedError


@app.get("/schedule")
def get_schedule(parser_name: str):
    engine = db_connect()
    create_table(engine)
    session = sessionmaker(bind=engine)()
    workItems = session.query(WorkItem).filter_by(completed=False, spiderName=parser_name)

    result = ScheduleOutput(parser_name=parser_name)

    for item in workItems:
        result.schedule.append(ScheduledRun(id=item.id, run_date=item.next_run, run_period=item.period))

    return result


@app.get("/full_schedule")
def get_all_schedule():
    engine = db_connect()
    create_table(engine)
    session = sessionmaker(bind=engine)()
    wrongGrouping = session.query(WorkItem).filter_by(completed=False).order_by(WorkItem.spiderName).all()

    workItemsByName = {k: list(g) for k, g in groupby(wrongGrouping, attrgetter('spiderName'))}
    result = {}

    for parser_name in workItemsByName:
        result[parser_name] = ScheduleOutput(parser_name=parser_name)

        for item in workItemsByName[parser_name]:
            result[parser_name]\
                .schedule.append(ScheduledRun(id=item.id, run_date=item.next_run, run_period=item.period))

    return result


@app.delete("/remove_from_schedule")
def remove_from_schedule(work_item_id: int):
    raise NotImplementedError


@app.get("/data")
def data(parser_name: str):
    engine = db_connect()
    create_table(engine)
    repo = RepositoryFactory().create(parser_name)

    result = repo.get_all_entries()

    return result

@app.get("/data_in_time_interval")
def data(parser_name: str):
    raise NotImplementedError

if __name__ == "__main__":
    os.environ.setdefault('SCRAPY_SETTINGS_MODULE', "parser_orchestrator.settings")
    uvicorn.run(app, host="0.0.0.0", port=8000)

