from datetime import datetime, timedelta
from typing import Optional

from pydantic import BaseModel
from pydantic.json import timedelta_isoformat, isoformat #TODO


class ScheduleInput(BaseModel):
    run_date: datetime
    run_period: Optional[timedelta]

    class Config:
        json_encoders = {
            datetime: isoformat,
            timedelta: timedelta_isoformat,
        }


class ScheduledRun(BaseModel):
    id: int
    run_date: datetime
    run_period: Optional[timedelta]

    class Config:
        json_encoders = {
            datetime: isoformat,
            timedelta: timedelta_isoformat,
        }


class ScheduleOutput:
    def __init__(self, parser_name: str):
        self.parser_name = parser_name
        self.schedule = []
