from scrapy.utils.project import get_project_settings
from sqlalchemy.orm import sessionmaker

from parser_orchestrator.models.SpiderMap import SpiderMap
from parser_orchestrator.models.base import db_connect


def get_project_settings_modified():
    engine = db_connect()
    session = sessionmaker(bind=engine)()

    settings = get_project_settings()

    for module in session.query(SpiderMap).all():
        settings["SPIDER_MODULES"].append(module.moduleName)
    return settings
