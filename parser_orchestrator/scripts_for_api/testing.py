'''import datetime

from scrapy.crawler import CrawlerProcess, CrawlerRunner
from scrapy.utils.project import get_project_settings
from sqlalchemy.orm import relationship, sessionmaker
from twisted.internet import reactor
from twisted.internet.task import deferLater

from papser_orchestrator.models.base import Base, db_connect, create_table
from sqlalchemy import Column, DateTime, NVARCHAR, ForeignKey, Table, Interval, Boolean
from sqlalchemy import (
    Integer)

from papser_orchestrator.scripts_for_api.long_running_task import WorkItem
from papser_orchestrator.scripts_for_api.run_spider import runSpider




engine = db_connect()
create_table(engine)
sessionmaker = sessionmaker(bind=engine)
session = sessionmaker()
session.add(
    WorkItem(
        spiderName="test",
        next_run=datetime.datetime.now(),
        period=datetime.timedelta(minutes=1)))
session.commit()
session.close()'''



