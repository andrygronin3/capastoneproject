import os
import sys

from parser_orchestrator.models.SpiderMap import SpiderMap
from parser_orchestrator.models.base import db_connect, create_table
from sqlalchemy.orm import sessionmaker
from scrapy import spiderloader
from scrapy.utils import project

from parser_orchestrator.scripts_for_api.get_project_settings_modified import get_project_settings_modified

if __name__ == "__main__":
    engine = db_connect()
    create_table(engine)
    sessionmaker = sessionmaker(bind=engine)
    session = sessionmaker()

    settings = get_project_settings_modified()
    for directory in next(os.walk('../parsers'))[1][:-1]:
        settings["SPIDER_MODULES"].append('parser_orchestrator.parsers.' + directory + '.spider')

    spider_loader = spiderloader.SpiderLoader.from_settings(settings)
    spider_names = spider_loader.list()

    registered_spiders = [x.spiderName for x in session.query(SpiderMap).all()]

    for i in range(3):
        name = spider_names[i]
        if name not in registered_spiders:
            newRepoClassName = input(f"Located an unregistered spider: {name} "
                                     f"\n Insert Repository classname for this spider ")
            session.add(
                SpiderMap(
                    spiderName=name,
                    className=newRepoClassName,
                    moduleName=spider_loader.spider_modules[i])
            )
    session.commit()
    session.close()

