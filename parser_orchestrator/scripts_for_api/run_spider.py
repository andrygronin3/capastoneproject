import logging

from scrapy.crawler import CrawlerProcess
from sqlalchemy.orm import sessionmaker

from parser_orchestrator.long_running_task.SqlLogHandler import SQLiteHandler
from parser_orchestrator.models.base import db_connect, create_table
from parser_orchestrator.scripts_for_api.get_project_settings_modified import get_project_settings_modified


def runSpider(name):
    process = CrawlerProcess(get_project_settings_modified())
    process.crawl(name)
    process.start() # the script will block here until the crawling is finished

#name = "quotes"
name = "EacRegistry"
#name = "NostroyRegistry"

engine = db_connect()
create_table(engine)
session = sessionmaker(bind=engine)

logger = logging.getLogger()
handler = SQLiteHandler(session(), name, None)
handler.setFormatter(logging.Formatter("%(asctime)s"))
logger.addHandler(handler)

runSpider(name)

