from sqlalchemy import Column, DateTime, NVARCHAR, ForeignKey, Table, Interval, Boolean
from sqlalchemy import (
    Integer)

from parser_orchestrator.models.WorkItem import WorkItem
from parser_orchestrator.models.base import Base, db_connect, create_table


class LogEntry(Base):
    __tablename__ = "logEntry"

    id = Column(Integer, primary_key=True)
    spiderName = Column(NVARCHAR(1000))
    asctime = Column(DateTime)
    level = Column(NVARCHAR(1000))
    message = Column(NVARCHAR(10000))
    workItemId = Column(Integer, ForeignKey(WorkItem.id), nullable=True)
