from abc import ABCMeta, abstractmethod

class IRegistryDataRepository:
    __metaclass__ = ABCMeta

    @abstractmethod
    def get_all_entries(self): raise NotImplementedError

    #@abstractmethod
    #def get_in_datetime_interval(self, beginning: datetime, ending: datetime): raise NotImplementedError


'''class MyServer(IInterface):
    def show(self):
        print 'Hello, World 2!'

class MyBadServer(object):
    def show(self):
        print 'Damn you, world!'


class MyClient(object):

    def __init__(self, server):
        if not isinstance(server, IInterface): raise Exception('Bad interface')
        if not IInterface.version() == '1.0': raise Exception('Bad revision')

        self._server = server


    def client_show(self):
        self._server.show()'''
