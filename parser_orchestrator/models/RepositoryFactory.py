from sqlalchemy.orm import sessionmaker
import os
import importlib

from parser_orchestrator.models.SpiderMap import SpiderMap
from parser_orchestrator.models.base import db_connect


def get_repo_files(src):
    cwd = os.getcwd() # Current Working directory
    py_files = []
    for root, dirs, files in os.walk(src):
        for file in files:
            if file == "repository.py":
                py_files.append(os.path.join(cwd, root, file))
    return py_files


def dynamic_import(module_name, py_path):
    module_spec = importlib.util.spec_from_file_location(module_name, py_path)
    module = importlib.util.module_from_spec(module_spec)
    module_spec.loader.exec_module(module)
    return module


def dynamic_import_from_src(src, star_import = False):
    my_py_files = get_repo_files(src)
    for py_file in my_py_files:
        module_name = os.path.split(py_file)[-1].strip(".py")
        imported_module = dynamic_import(module_name, py_file)
        if star_import:
            for obj in dir(imported_module):
                globals()[obj] = imported_module.__dict__[obj]
        else:
            globals()[module_name] = imported_module
    return

class RepositoryFactory:
    def create(self, parser_name):
        engine = db_connect()
        session = sessionmaker(bind=engine)()
        repository_map = session.query(SpiderMap).filter_by(spiderName = parser_name).first()
        session.close()

        if repository_map is not None:
            dynamic_import_from_src("../parsers", star_import=True)

            klass = globals()[repository_map.className]
            repo = klass()
            return repo
        else:
            raise Exception("Wrong parser name or parser isn't registered")
