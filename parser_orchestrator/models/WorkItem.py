from sqlalchemy import Column, DateTime, NVARCHAR, ForeignKey, Table, Interval, Boolean
from sqlalchemy import (
    Integer)
from parser_orchestrator.models.base import Base, db_connect, create_table


class WorkItem(Base):
    __tablename__ = "workItems"

    id = Column(Integer, primary_key=True)
    spiderName = Column(NVARCHAR(1000))
    next_run = Column(DateTime)
    period = Column(Interval, nullable=True)
    completed = Column(Boolean, default=False)