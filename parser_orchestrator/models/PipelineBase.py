from scrapy.exceptions import DropItem
from sqlalchemy.orm import sessionmaker

from parser_orchestrator.models.base import db_connect, create_table

import logging

class BasePipeline(object):

    def __init__(self):
        engine = db_connect()
        create_table(engine)
        self.Session = sessionmaker(bind=engine)
        logging.info("****DuplicatesPipeline: database connected****")

    def process_item_base(self, item, spider, model_type, selector):
        session = self.Session()
        query = session.query(model_type)
        extractedModel = query.filter(selector).first()
        if extractedModel is not None:
            session.close()
            raise DropItem("Duplicate item found: %s" % item[spider.id_field_name])
        else:
            session.close()
            return item

        return item