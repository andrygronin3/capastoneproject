from sqlalchemy import Column, NVARCHAR

from parser_orchestrator.models.base import Base


class SpiderMap(Base):
    __tablename__ = "SpiderMap"
    spiderName = Column(NVARCHAR(1000), primary_key=True)
    className = Column(NVARCHAR(1000))
    moduleName = Column(NVARCHAR(1000))

